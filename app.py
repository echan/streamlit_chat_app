# adapted from https://docs.streamlit.io/knowledge-base/tutorials/build-conversational-apps
import os
import requests
import streamlit as st
import time

FALCON7B_INFERENCE_ENDPOINT_URL  = st.secrets['FALCON7B_INFERENCE_ENDPOINT_URL']  # put hugging face inference endpoint url here
MISTRAL7B_INFERENCE_ENDPOINT_URL = st.secrets['MISTRAL7B_INFERENCE_ENDPOINT_URL']
HUGGINGFACE_TOKEN                = st.secrets['HUGGINGFACE_TOKEN']                # put hugging face token here

st.title("Chat")

model_to_use = st.selectbox(
    label = 'Select Your Model'
    , options = ['Falcon 7B', 'Mistral 7B'] 

)

model_to_endpoint_url_dict = {
    'Falcon 7B': FALCON7B_INFERENCE_ENDPOINT_URL
    , 'Mistral 7B': MISTRAL7B_INFERENCE_ENDPOINT_URL
}

context_input = st.text_area(label = 'Context', value = '', height = 6)

INFERENCE_ENDPOINT_URL = model_to_endpoint_url_dict[model_to_use]

headers = {"Authorization": f"Bearer {HUGGINGFACE_TOKEN}"}

# HuggingFace API request function
def query(payload):
    response = requests.post(
        INFERENCE_ENDPOINT_URL
        , headers = headers
        , json = payload)
    return response.json()

# # Initialize chat history
# if "messages" not in st.session_state:
#     st.session_state.messages = []

# Store LLM generated responses
if "messages" not in st.session_state.keys():
    st.session_state.messages = [{"role": "assistant", "content": "How may I assist you today?"}]

# Display chat messages from history on app rerun
for message in st.session_state.messages:
    with st.chat_message(message["role"]):
        st.markdown(message["content"])

# Accept user input
if prompt := st.chat_input("Type chat message here"):
    # Add user message to chat history
    st.session_state.messages.append({"role": "user", "content": prompt})
    # Display user message in chat message container
    with st.chat_message("user"):
        st.markdown(prompt)

# Generate a new response if last message is not from assistant
if st.session_state.messages[-1]["role"] != "assistant":
    # Display assistant response in chat message container
    with st.chat_message("assistant"):
        # this shows the app is thinking
        with st.spinner("Getting your response ..."):
            # if context is not empty, prepend it to the prompt
            if context_input != '':
                prompt = f'Context: {context_input}\n\nUse the context to answer the following. {prompt}'
            payload = {
                'inputs': prompt
                , 'parameters': {          # https://huggingface.co/docs/api-inference/detailed_parameters
                    'max_new_tokens': 512 
                }
            }
            response =  query(payload) # make an api call
            placeholder = st.empty()
            full_response = ''

            # Simulate stream of response with milliseconds delay
            for chunk in response[0]['generated_text']:
                full_response += chunk
                time.sleep(0.01)
                # Add a blinking cursor to simulate typing
                placeholder.markdown(full_response + "▌")

            placeholder.markdown(full_response) 
        st.session_state.messages.append({"role": "assistant", "content": full_response})