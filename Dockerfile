FROM python:3.10-slim
LABEL org.opencontainers.image.authors="echan@nfa.futures.org"
WORKDIR /app
COPY requirements.txt ./requirements.txt
RUN mkdir ~/.streamlit  
COPY .streamlit/secrets.toml ~/.streamlit/secrets.toml
RUN pip install -r requirements.txt
EXPOSE 8501
COPY . /app     
ENTRYPOINT ["streamlit", "run"]
CMD ["app.py"]